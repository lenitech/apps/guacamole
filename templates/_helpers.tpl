{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common daemon labels
*/}}
{{- define "chart.daemon.labels" -}}
helm.sh/chart: {{ include "chart.chart" . }}
{{ include "chart.daemon.selectorLabels" . }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector daemon labels
*/}}
{{- define "chart.daemon.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}-daemon
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/component: daemon
app.kubernetes.io/part-of: {{ include "chart.name" . }}
{{- end }}

{{/*
Common web labels
*/}}
{{- define "chart.web.labels" -}}
helm.sh/chart: {{ include "chart.chart" . }}
{{ include "chart.web.selectorLabels" . }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector web labels
*/}}
{{- define "chart.web.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}-web
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/component: web
app.kubernetes.io/part-of: {{ include "chart.name" . }}
{{- end }}
